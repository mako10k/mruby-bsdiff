#include <mruby.h>
#include <mruby/string.h>
#include <bsdiff.h>
#include <bspatch.h>
#include <stdlib.h>

struct bsdiff_ctx
{
  mrb_state *mrb;
  FILE *fp;
};

static inline int
write_int (FILE * fp, int64_t i)
{
  if (i >= 0)
    while (1)
      {
	if (fputc ((i & 127) | (i < 64 ? 0 : 128), fp) == EOF)
	  return -1;
	if (i < 64)
	  return 0;
	i >>= 7;
      }
  else
    while (1)
      {
	if (fputc ((i & 127) | (i >= -64 ? 0 : 128), fp) == EOF)
	  return -1;
	if (i >= -64)
	  return 0;
	i = (i >> 7) | ~((int64_t) (-1) >> 7);
      }
}

static inline int
write_uint (FILE * fp, uint64_t i)
{
  while (1)
    {
      if (fputc ((i & 127) | (i < 128 ? 0 : 128), fp) == EOF)
	return -1;
      if (i < 128)
	return 0;
      i >>= 7;
    }
}

static inline int
read_int (FILE * fp, int64_t * ip)
{
  int i, b = 0;
  int64_t r = 0;

  do
    {
      if ((i = fgetc (fp)) == EOF)
	return -1;
      r += (i & 127) << (7 * b++);
    }
  while (i & 128);
  if (i & 64)
    r |= ~((uint64_t) 0xffffffffffffffff >> (64 - 7 * b));
  *ip = r;
  return 0;
}

static inline int
read_uint (FILE * fp, uint64_t * ip)
{
  int i, b = 0;
  uint64_t r = 0;

  do
    {
      if ((i = fgetc (fp)) == EOF)
	return -1;
      r += (i & 127) << (7 * b++);
    }
  while (i & 128);
  *ip = r;
  return 0;
}

static int
bs_write (struct bsdiff_stream *bs, const void *buf, int size)
{
  struct bsdiff_ctx *ctx = bs->opaque;
  size_t wsize = fwrite (buf, 1, size, ctx->fp);

  return size != wsize;
}

static int
bs_read (const struct bspatch_stream *bs, void *buf, int size)
{
  struct bsdiff_ctx *ctx = bs->opaque;
  size_t rsize = fread (buf, 1, size, ctx->fp);

  return size != rsize;
}

static mrb_value
mrb_bsdiff_bsdiff (mrb_state * mrb, mrb_value self)
{
  void *old, *new;
  char *patch = NULL;
  size_t oldsize, patchsize;
  mrb_int newsize;
  struct bsdiff_ctx ctx;
  struct bsdiff_stream bs;
  mrb_value ret;

  if (!mrb_string_p (self))
    mrb_raise (mrb, E_RUNTIME_ERROR, "canot make bsdiff (expect string)");
  old = RSTRING_PTR (self);
  oldsize = RSTRING_LEN (self);
  mrb_get_args (mrb, "s", &new, &newsize);
  ctx.mrb = mrb;
  ctx.fp = open_memstream (&patch, &patchsize);
  if (ctx.fp == NULL)
    mrb_raise (mrb, E_RUNTIME_ERROR, "cannot make bsdiff");
  bs.opaque = &ctx;
  bs.malloc = &malloc;
  bs.free = &free;
  bs.write = &bs_write;
  if (write_uint (ctx.fp, newsize))
    {
      fclose (ctx.fp);
      if (patch)
	free (patch);
      mrb_raise (mrb, E_RUNTIME_ERROR, "cannot make bsdiff");
    }
  if (bsdiff (old, oldsize, new, newsize, &bs))
    {
      fclose (ctx.fp);
      if (patch)
	free (patch);
      free (patch);
      mrb_raise (mrb, E_RUNTIME_ERROR, "cannot make bsdiff");
    }
  fclose (ctx.fp);
  ret = mrb_str_new (mrb, patch, patchsize);	// 例外発生時に patch が free されない
  if (patch)
    free (patch);
  return ret;
}

static mrb_value
mrb_bsdiff_bspatch (mrb_state * mrb, mrb_value self)
{
  void *old, *new, *patch;
  size_t oldsize;
  uint64_t newsize;
  mrb_int patchsize;
  struct bsdiff_ctx ctx;
  struct bspatch_stream bs;
  mrb_value ret;

  if (!mrb_string_p (self))
    mrb_raise (mrb, E_RUNTIME_ERROR, "canot apply bspatch (expect string)");
  old = RSTRING_PTR (self);
  oldsize = RSTRING_LEN (self);
  mrb_get_args (mrb, "s", &patch, &patchsize);
  ctx.mrb = mrb;
  ctx.fp = fmemopen (patch, patchsize, "r");
  if (ctx.fp == NULL)
    mrb_raise (mrb, E_RUNTIME_ERROR, "cannot apply bspatch");
  bs.opaque = &ctx;
  bs.read = &bs_read;
  newsize = 0;
  if (read_uint (ctx.fp, &newsize))
    {
      fclose (ctx.fp);
      mrb_raise (mrb, E_RUNTIME_ERROR,
		 "cannot apply bspatch (malformed patch data)");
    }
  ret = mrb_str_new_capa (mrb, newsize);	// 例外発生時に ctx.fp が close されない
  new = RSTRING_PTR (ret);
  if (bspatch (old, oldsize, new, newsize, &bs))
    {
      fclose (ctx.fp);
      mrb_raise (mrb, E_RUNTIME_ERROR,
		 "cannot apply bspatch (malformed patch data or invalid combination between source and patch)");
    }
  fclose (ctx.fp);
  RSTR_SET_LEN (mrb_str_ptr (ret), newsize);
  return ret;
}

static inline void
offtout (int64_t c, uint8_t * buf)
{
  if (c < 0)
    c = -c | 0x8000000000000000;
  buf[0] = c;
  c >>= 8;
  buf[1] = c;
  c >>= 8;
  buf[2] = c;
  c >>= 8;
  buf[3] = c;
  c >>= 8;
  buf[4] = c;
  c >>= 8;
  buf[5] = c;
  c >>= 8;
  buf[6] = c;
  c >>= 8;
  buf[7] = c;
}

static inline int64_t
offtin (uint8_t * buf)
{
  int64_t c;

  c = 0;
  c = buf[7] & 127;
  c = (c << 8) | buf[6];
  c = (c << 8) | buf[5];
  c = (c << 8) | buf[4];
  c = (c << 8) | buf[3];
  c = (c << 8) | buf[2];
  c = (c << 8) | buf[1];
  c = (c << 8) | buf[0];
  if (buf[7] & 0x80)
    c *= -1;
  return c;
}

static inline int
copy_data (FILE * ifp, FILE * ofp, size_t size)
{
  while (size--)
    {
      int c;

      if ((c = getc (ifp)) == EOF)
	return -1;
      if (fputc (c, ofp) == EOF)
	return -1;
    }
  return 0;
}

static mrb_value
mrb_bsdiff_compress (mrb_state * mrb, mrb_value self)
{
  char *new;
  size_t newsize;
  FILE *ifp, *ofp;
  mrb_value ret;
  uint64_t xnewsize = 0;

  if (!mrb_string_p (self))
    mrb_raise (mrb, E_RUNTIME_ERROR,
	       "cannot compress bspatch (expect string)");
  ifp = fmemopen (RSTRING_PTR (self), RSTRING_LEN (self), "r");
  if (ifp == NULL)
    mrb_raise (mrb, E_RUNTIME_ERROR, "cannot compress bspatch");
  new = NULL;
  ofp = open_memstream (&new, &newsize);
  if (ofp == NULL)
    {
      fclose (ifp);
      mrb_raise (mrb, E_RUNTIME_ERROR, "cannot compress bspatch");
    }
  if (read_uint (ifp, &xnewsize))
    {
      fclose (ifp);
      if (new)
	free (new);
      fclose (ofp);
      mrb_raise (mrb, E_RUNTIME_ERROR,
		 "cannot compress bspatch (malformed patch data)");
    }
  if (write_uint (ofp, xnewsize))
    {
      fclose (ifp);
      if (new)
	free (new);
      fclose (ofp);
      mrb_raise (mrb, E_RUNTIME_ERROR, "cannot compress bspatch");
    }
  while (!feof (ifp))
    {
      uint8_t buf[24];
      int64_t ctrl[3];
      int i;
      size_t r;

      if ((r = fread (buf, 1, 24, ifp)) == 0)
	{
	  if (ferror (ifp))
	    {
	      fclose (ifp);
	      if (new)
		free (new);
	      fclose (ofp);
	      mrb_raise (mrb, E_RUNTIME_ERROR, "cannot compress bspatch");
	    }
	  break;		// EOF
	}
      if (r < 24)
	{
	  fclose (ifp);
	  if (new)
	    free (new);
	  fclose (ofp);
	  mrb_raise (mrb, E_RUNTIME_ERROR,
		     "cannot compress bspatch (malformed bspatch)");
	}
      for (i = 0; i < 3; i++)
	{
	  ctrl[i] = offtin (buf + i * 8);
	  if (write_int (ofp, ctrl[i]))
	    {
	      fclose (ifp);
	      if (new)
		free (new);
	      fclose (ofp);
	      mrb_raise (mrb, E_RUNTIME_ERROR, "cannot compress bspatch");
	    }
	}
      if (copy_data (ifp, ofp, ctrl[0] + ctrl[1]))
	{
	  fclose (ifp);
	  if (new)
	    free (new);
	  fclose (ofp);
	  mrb_raise (mrb, E_RUNTIME_ERROR,
		     "cannot compress bspatch (malformed bspatch)");
	}
    }
  fclose (ifp);
  fclose (ofp);
  ret = mrb_str_new (mrb, new, newsize);	// 例外発生時に new が free されない
  if (new)
    free (new);
  return ret;
}

static mrb_value
mrb_bsdiff_decompress (mrb_state * mrb, mrb_value self)
{
  char *new;
  size_t newsize;
  FILE *ifp, *ofp;
  mrb_value ret;
  uint64_t xnewsize = 0;

  if (!mrb_string_p (self))
    mrb_raise (mrb, E_RUNTIME_ERROR,
	       "cannot decompress bspatch (expect string)");
  ifp = fmemopen (RSTRING_PTR (self), RSTRING_LEN (self), "r");
  if (ifp == NULL)
    mrb_raise (mrb, E_RUNTIME_ERROR, "cannot decompress bspatch");
  new = NULL;
  ofp = open_memstream (&new, &newsize);
  if (ofp == NULL)
    {
      fclose (ifp);
      mrb_raise (mrb, E_RUNTIME_ERROR, "cannot decompress bspatch");
    }
  if (read_uint (ifp, &xnewsize))
    {
      fclose (ifp);
      if (new)
	free (new);
      fclose (ofp);
      mrb_raise (mrb, E_RUNTIME_ERROR,
		 "cannot decompress bspatch (malformed compressed bspatch)");
    }
  if (write_uint (ofp, xnewsize))
    {
      fclose (ifp);
      if (new)
	free (new);
      fclose (ofp);
      mrb_raise (mrb, E_RUNTIME_ERROR, "cannot decompress bspatch");
    }
  while (!feof (ifp))
    {
      uint8_t buf[8];
      int64_t ctrl[3];
      int i;

      for (i = 0; i < 3; i++)
	{
	  ctrl[i] = 0;
	  if (read_int (ifp, ctrl + i))
	    {
	      if (ferror (ifp))
		{
		  fclose (ifp);
		  if (new)
		    free (new);
		  fclose (ofp);
		  mrb_raise (mrb, E_RUNTIME_ERROR,
			     "cannot decompress bspatch");
		}
	      break;		// EOF
	    }
	  offtout (ctrl[i], buf);
	  if (fwrite (buf, 1, 8, ofp) != 8)
	    {
	      fclose (ifp);
	      if (new)
		free (new);
	      fclose (ofp);
	      mrb_raise (mrb, E_RUNTIME_ERROR, "cannot decompress bspatch");
	    }
	}
      if (i < 3)
	break;
      if (copy_data (ifp, ofp, ctrl[0] + ctrl[1]))
	{
	  fclose (ifp);
	  if (new)
	    free (new);
	  fclose (ofp);
	  mrb_raise (mrb, E_RUNTIME_ERROR,
		     "cannot decompress bspatch (malformed compressed bspatch)");
	}
    }
  fclose (ofp);
  ret = mrb_str_new (mrb, new, newsize);	// 例外発生時に new が free されない
  if (new)
    free (new);
  return ret;
}

void
mrb_mruby_bsdiff_gem_init (mrb_state * mrb)
{
  mrb_define_method (mrb, mrb->string_class, "bsdiff", mrb_bsdiff_bsdiff,
		     MRB_ARGS_REQ (1));
  mrb_define_method (mrb, mrb->string_class, "bspatch", mrb_bsdiff_bspatch,
		     MRB_ARGS_REQ (1));
  mrb_define_method (mrb, mrb->string_class, "bsdiff_compress",
		     mrb_bsdiff_compress, MRB_ARGS_NONE ());
  mrb_define_method (mrb, mrb->string_class, "bsdiff_decompress",
		     mrb_bsdiff_decompress, MRB_ARGS_NONE ());
}

void
mrb_mruby_bsdiff_gem_final (mrb_state * mrb)
{
}
