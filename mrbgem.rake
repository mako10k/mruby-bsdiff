MRuby::Gem::Specification.new('mruby-bsdiff') do |spec|
  spec.license = 'MIT'
  spec.authors = 'mako10k'
  spec.summary = 'mruby bsdiff / bspatch'
  spec.version = '0.0.1'

  require 'open3'

  bsdiff_dir = "#{build_dir}/bsdiff"

  def run_command env, command
    STDOUT.sync = true
    puts "build: [exec] #{command}"
    Open3.popen2e(env, command) do |stdin, stdout, thread|
      print stdout.read
      fail "#{command} failed" if thread.value != 0
    end
  end

  FileUtils.mkdir_p build_dir

  if ! File.exists? bsdiff_dir
    Dir.chdir(build_dir) do
      e = {}
      run_command e, 'git clone https://github.com/mendsley/bsdiff.git'
    end
  end

  if ! File.exists? "#{bsdiff_dir}/configure"
    Dir.chdir(bsdiff_dir) do
      e = {}
      run_command e, "./autogen.sh"
      run_command e, "rm -f #{bsdiff_dir}/Makefile"
    end
  end
  if ! File.exists? "#{bsdiff_dir}/Makefile"
    Dir.chdir(bsdiff_dir) do
      e = {}
      run_command e, "./configure"
      run_command e, "rm -f #{bsdiff_dir}/bsdiff-bsdiff.o"
      run_command e, "rm -f #{bsdiff_dir}/bspatch-bspatch.o"
    end
  end  
  Dir.chdir(bsdiff_dir) do
    e = {}
    run_command e, "make bsdiff_CFLAGS= bsdiff-bsdiff.o"
    run_command e, "make bspatch_CFLAGS= bspatch-bspatch.o"
    run_command e, <<"SCRIPT"
if [ ! -e libbsdiff.a -o libbsdiff.a -ot bsdiff-bsdiff.o -o libbsdiff.a -ot bspatch-bspatch.o ]; then
  rm -f libbsdiff.a
  #{spec.archiver.command} rcs libbsdiff.a bsdiff-bsdiff.o bspatch-bspatch.o
fi
SCRIPT
  end

  spec.cc.include_paths << bsdiff_dir
  spec.linker.library_paths << bsdiff_dir
  spec.linker.libraries << 'bsdiff'
end
